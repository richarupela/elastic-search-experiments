from datetime import datetime

from elasticsearch import Elasticsearch
from elasticsearch import helpers

es = Elasticsearch()
j = 0
actions = []
while (j <= 1):
    action = {
    "_index": "arraytest",
    "_type": "test",
    "_id": j,
    "_source": {
        "terms": "data" + str(j),
        "timestamp": datetime.now(),
        "tags": ['python', 'go']
    }
  }
    actions.append(action)
    j += 1

helpers.bulk(es, actions)

print es.count(index='arraytest')