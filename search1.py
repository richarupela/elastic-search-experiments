from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

client = Elasticsearch()

response = client.search(
    index="test_data",
    body={
        "query": {
          "match": {
            "hobbies": "TV"
          }
        }
    }
)
print "==response==", response

for hit in response['hits']['hits']:
    print "==hit source==", hit['_source']
