from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search

client = Elasticsearch()

s = Search(using=client, index="tutorial")
s = Search().using(client).query("match", title="python")
# s1 = Search().using(client).query("match", title="Welcome")
response = s.execute()
# response1 = s1.execute()
print response
for hit in s:
    print(hit.title)
# for hit in s1:
#     print(hit.title)
