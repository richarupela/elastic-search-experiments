from datetime import datetime

from elasticsearch import Elasticsearch
from elasticsearch import helpers

es = Elasticsearch()

actions = [
  {
    "_index": "test-index",
    "_type": "tests",
    "_id": j,
    "_source": {
        "terms": "data" + str(j),
        "timestamp": datetime.now(),
        "tags": ['python', 'go']
    }
  }
  for j in range(0, 10)
]

helpers.bulk(es, actions)

print es.count(index='test-index')